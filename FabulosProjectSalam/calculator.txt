using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            float result = 0;

            if (args.Length == 3)
            {

                float a = float.Parse(args[0]);
                float b = float.Parse(args[2]);
                string op = args[1];

                switch (args[1])
                {
                    case "+":
                        result = a + b;
                        Console.WriteLine(a + " + " + b + " = " + result);
                        break;
                    case "-":
                        result = a - b;
                        Console.WriteLine(a + " - " + b + " = " + result);
                        break;
                    case "*":
                        result = a * b;
                        Console.WriteLine(a + " * " + b + " = " + result);
                        break;
                    case "/":
                        result = a / b;
                        Console.WriteLine(a + " / " + b + " = " + result);
                        break;
                    default:
                        Console.WriteLine(args[1] + " is not a valid operation");
                        break;
                }

                Console.ReadKey();


            
                Console.WriteLine("Result is: " + result);
            }
            else
            {
                Console.WriteLine("3 args are needed!");
            }

        }
    }
}


2 + 456
