using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            ReverseString(args[0]);
        }

        public static void ReverseString(string s)
        {
            string Input = s;
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);

            if (Input == new string(arr))
            {
                Console.WriteLine("Is palindrom");
             
            }
            else
            {
                Console.WriteLine("Is not palindrom!");
            }
            
            Console.WriteLine(arr);
            Console.ReadKey();
        }
    }
}
